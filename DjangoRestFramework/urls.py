"""DjangoRestFramework URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from django.urls import include
from django.urls import path
from rest_framework.routers import DefaultRouter

from rest_framework.documentation import include_docs_urls
from rest_framework.schemas import get_schema_view

from refs.router import router as refs
from uat.router import router as uat


router = DefaultRouter()
router.registry.extend(refs.registry)
router.registry.extend(uat.registry)

urlpatterns = [
    path('admin/', admin.site.urls),
    path('dev/', include('dev.route')),
    # url(r'^api-token-auth/', obtain_jwt_token),
    path('api-auth/', include('rest_framework.urls')),
    # url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^api/', include(router.urls)),
    url(r'^api/docs/', include_docs_urls(title="API")),




    # 当没有匹配到任何url的时候 默认主页
    path('', include(router.urls)),
    # re_path('^$', include(router.urls)),
    # url('^', include(router.urls)),
    # url('^$', include(router.urls)),

]
