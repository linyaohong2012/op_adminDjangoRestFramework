# Create your views here.

# HttpResponse 返回字符串
from django.http import HttpResponse  # from django.shortcuts import HttpResponse
# JsonResponse 返回json格式信息
from django.http import JsonResponse
# 跳转 redirect 返回其他接口
from django.shortcuts import redirect
# render返回html
from django.shortcuts import render
from django.urls import reverse

# 自定义实现使用 . 操作字典
from libs import AttrDict


def test01(request):
    return HttpResponse("test01-HttpResponse")


def test02(request):
    # 返回JSON
    data = {"code": 0, "msg": "JsonResponse"}
    return JsonResponse(data)  # django 内置的方法,只支持 字典


def test03(request):
    # 准许返回列表
    m = [1, 2, 3]
    return JsonResponse(m, safe=False)  # django 内置的方法,safe=False 不进行检测,也可以返回列表


def test04(request):
    # 返回html文件内容
    return render(request, "test04.html")


def test051(request):
    # 添加成功后直接跳转到用户列表页
    return redirect("/dev/test01")     # 正常跳转


def test052(request):
    # 添加成功后直接跳转到用户列表页
    redirect_url = reverse("test0001")  # url反向解析
    return redirect(redirect_url)


def test06(request):
    return HttpResponse("test06")


def test07(request, pk):  # 从访问路径里获取参数
    print(pk)
    return HttpResponse(pk)


def test08(request, pk):  # 接收 url里传来的路径参数
    print(type(pk))
    return HttpResponse(pk)


def test09(request, month, year):  # 接收 url里传来的路径参数
    data = str(year) + '/' + str(month)
    return HttpResponse(data)


def test10(request, year):  # 接收 url里传来的路径参数
    print(type(year))
    return HttpResponse(year)


def test11(request, month, year):  # 接收 url里传来的路径参数
    print(month)
    print(year)
    data = str(year) + '/' + str(month)
    return HttpResponse(data)


def test12(request, year, month, slug):  # 接收 url里传来的路径参数
    data = str(year) + '/' + str(month) + '/' + str(slug)
    return HttpResponse(data)


# 关键字参数
def test13(request, **kwargs):  # 接收 url里传来的路径参数
    print(kwargs)
    content = AttrDict(kwargs)
    test = content.year
    print(test)
    # print(kwargs['year'])
    return JsonResponse(kwargs)
