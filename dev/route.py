# -*- coding:UTF-8 -*-
# Author  : LinYaoHong
# Date    : 2020/11/19 12:21 下午
# Filename: router.py
# Tools   : PyCharm
"""

    在新版本Django2.x中，
    url的路由表示用path和re_path代替，
    django1.x版本的 from django.conf.urls import url,include
    Django2.x中的   from django.urls import path, re_path, include
    官方文档 : https://docs.djangoproject.com/zh-hans/2.2/topics/http/urls/

    # 这个常量默认为True，就是假如你没有添加 /，django会自动加上/
    # APPEND_SLASH = False

"""
from django.conf.urls import url
from django.urls import path
from django.urls import re_path

from dev import views as dev

urlpatterns = [

    url('^test01/$', dev.test01, name="test0001"),  # /dev/test01/
    url('^test02/', dev.test02),  # 返回 json格式 如果不加 $  /dev/test02/xxx/xxx也可以正常访问
    path('test03/', dev.test03),  # 返回列表
    path('test04/', dev.test04),  # 返回html
    path('test051/', dev.test051),  # 跳转url

    path('test052/', dev.test052),  # 跳转url
    # redirect_url = reverse("test0001")  # url反向解析
    # return redirect(redirect_url)

    # 不带()不会把参数传到后面的函数 django < 2.0
    url(r'^test06/[0-9]{4}/$', dev.test06),  # /dev/test06/1234/   /dev/test06/则 404

    # 带上() 则会把内容当作参数传到后端接收
    url(r'^test07/([0-9]{4})/$', dev.test07),  # def test07(request, pk)
    url(r'^test10/(?P<year>[0-9]{4})/$', dev.test10),
    re_path(r'^test11/(?P<year>[0-9]{4})/(?P<month>[0-9]{2})/$', dev.test11),

    # django >= 2.0  后端接收的参数为int
    path('test08/<int:pk>/', dev.test08),  # /dev/test08/221112/ 可以直接把 pk 当作参数来使用,比如 删除 详情 等使用场景代替 ?id=1
    path('test09/<int:year>/<int:month>/', dev.test09),  # 和08 类似 /dev/test09/221112/12122/
    path('test12/<int:year>/<int:month>/<slug:slug>/', dev.test12),  # /dev/test12/2003/03/building-a-django-site/

    # re_path  使用正则表达式 限制请求格式  /dev/test11/年/月/  后端接收的参数为 str 了解即可

    # 关键字参数
    path('test13/<int:year>/<int:month>/', dev.test13),

]
