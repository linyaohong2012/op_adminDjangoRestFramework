# -*- coding:UTF-8 -*-
# Author  : LinYaoHong
# Date    : 2020/11/21 1:39 上午
# Filename: re_test.py.py
# Tools   : PyCharm


"""

验证手机号码和邮箱

"""

import re

r = re.compile("1[3,5,8]\d{9}")
q = re.compile('\w+@\w+\.(com|cn|edu)')

qet = q.fullmatch("122123498@qqedu.com")
print(qet)
qet = q.fullmatch("122123498@qqedu.cm")
print(qet)

ret = r.fullmatch("18653902018")
ret2 = r.fullmatch("1865398")

if ret2 is not None:
    print(ret)
else:
    print("error")

