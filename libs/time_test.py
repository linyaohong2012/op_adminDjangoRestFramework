# -*- coding:UTF-8 -*-
# Author  : LinYaoHong
# Date    : 2020/11/30 11:18 上午
# Filename: time_test.py
# Tools   : PyCharm

#  求失效时间  优惠价失效时间
import datetime

now = datetime.datetime.now()
d7 = datetime.timedelta(days=7)  # 时间间隔
d365 = datetime.timedelta(weeks=52)

ret = now + d7
print(ret)
ret = now + d365
print(ret)
