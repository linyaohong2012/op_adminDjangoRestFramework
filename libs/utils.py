# -*- coding:UTF-8 -*-
# Author  : LinYaoHong
# Date    : 2020/11/20 11:24 上午
# Filename: utils.py
# Tools   : PyCharm


"""

    继承自dict，实现可以通过.来操作元素
    使用说明
    data = {
        'name': "lin",
        'age': 18,
        'addr': "山东"
    }
    tmp = AttrDict(data)
    print(tmp.name)
    print(tmp.age)

"""


class AttrDict(dict):
    def __setattr__(self, key, value):
        self.__setitem__(key, value)

    def __getattr__(self, item):
        return self.__getitem__(item)

    def __delattr__(self, item):
        self.__delitem__(item)
