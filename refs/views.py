# Create your views here.

from rest_framework import viewsets

from refs.models import (
    Project,
    Servers,
    DeployEnv,
    Dependencies,
)
from refs.serializers import (
    ProjectSerializer,
    ServersSerializer,
    DeployEnvSerializer,
    DependenciesSerializer
)


class ProjectViewSet(viewsets.ModelViewSet):
    queryset = Project.objects.all()
    serializer_class = ProjectSerializer


class ServersViewSet(viewsets.ModelViewSet):
    queryset = Servers.objects.all()
    serializer_class = ServersSerializer


class DeployEnvViewSet(viewsets.ModelViewSet):
    queryset = DeployEnv.objects.all()
    serializer_class = DeployEnvSerializer


class DependenciesViewSet(viewsets.ModelViewSet):
    queryset = Dependencies.objects.all()
    serializer_class = DependenciesSerializer
