from django.db import models


# Create your models here.

class Project(models.Model):
    domain = models.CharField(max_length=100, verbose_name="项目域名", null=True, blank=True)
    appid = models.CharField(max_length=30, verbose_name="APPID", unique=True, null=False)

    def __str__(self):
        return self.appid

    class Meta:
        db_table = "project"
        ordering = ['-id']
        verbose_name = "项目信息"
        verbose_name_plural = verbose_name


class DeployEnv(models.Model):
    env_name = models.CharField(max_length=20, verbose_name="环境名")
    branch = models.CharField(max_length=20, verbose_name="对应分支")

    def __str__(self):
        return self.env_name

    class Meta:
        db_table = 'deployenv'
        ordering = ['-id']
        verbose_name = "环境配置"
        verbose_name_plural = verbose_name


class Servers(models.Model):
    instance = models.CharField(max_length=40, verbose_name="实例名称", null=True)
    pri_ip = models.GenericIPAddressField(
        unique=True, protocol='IPv4', verbose_name="内网IP", default="", blank=True, null=True)

    def __str__(self):
        return self.instance

    class Meta:
        db_table = 'servers'
        ordering = ['-id']
        verbose_name = "服务器"
        verbose_name_plural = verbose_name


class Dependencies(models.Model):
    project = models.ForeignKey("Project", models.PROTECT, related_name='+', verbose_name="项目")
    env = models.ForeignKey("DeployEnv", models.PROTECT, related_name='+', verbose_name="环境")
    servers = models.ManyToManyField(to="Servers", related_name="dependencies", blank=True,
                                     verbose_name="服务器")

    class Meta:
        db_table = 'dependencies'
        unique_together = ('project', 'env')
        ordering = ['-id']
        verbose_name = "依赖关系"
        verbose_name_plural = verbose_name
