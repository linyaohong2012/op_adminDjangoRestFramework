# -*- coding:UTF-8 -*-
# Author  : LinYaoHong
# Date    : 2020/11/19 9:16 下午
# Filename: serializers.py
# Tools   : PyCharm

from rest_framework import serializers

from refs.models import (
    Servers,
    DeployEnv,
    Project,
    Dependencies
)


class ServersSerializer(serializers.ModelSerializer):
    operator = serializers.ReadOnlyField(source="operator.username")

    class Meta:
        model = Servers
        fields = '__all__'
        # fields = ('id', 'title', 'appid')
        # exclude = ()  表示不返回字段        三者取一
        # read_only_fields = () #设置只读字段 不接受用户修改


class DeployEnvSerializer(serializers.ModelSerializer):
    class Meta:
        model = DeployEnv
        fields = '__all__'


class ProjectSerializer(serializers.ModelSerializer):
    class Meta:
        model = Project
        fields = '__all__'


class DependenciesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Dependencies
        fields = '__all__'
        # depth = 1

    def to_representation(self, instance):
        representation = super(DependenciesSerializer, self).to_representation(instance)
        # representation['project'] = ProjectSerializer(instance.project).data
        project_obj = ProjectSerializer(instance.project).data
        representation['domain'] = project_obj['domain']
        representation['appid'] = project_obj['appid']
        env_obj = DeployEnvSerializer(instance.env).data
        representation['env_name'] = env_obj["env_name"]
        representation['servers'] = ServersSerializer(instance.servers, many=True).data
        data = []
        for ips in representation['servers']:
            print(ips["pri_ip"])
            data.append(ips["pri_ip"])
            print(data)
            # representation['deploy_hosts'] = data
            representation['deploy_hosts'] = ','.join(data)
        return representation
