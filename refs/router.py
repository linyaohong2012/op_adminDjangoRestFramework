# -*- coding:UTF-8 -*-
# Author  : LinYaoHong
# Date    : 2020/11/19 12:21 下午
# Filename: router.py
# Tools   : PyCharm

from rest_framework.routers import DefaultRouter

from refs.views import (
    ProjectViewSet,
    ServersViewSet,
    DeployEnvViewSet,
    DependenciesViewSet,
)

router = DefaultRouter()
urlpatterns = [
    router.register("project", ProjectViewSet, "project"),
    router.register("servers", ServersViewSet, "servers"),
    router.register("deployenv", DeployEnvViewSet, "deployenv"),
    router.register("dependencies", DependenciesViewSet, "dependencies")
]
