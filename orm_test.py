# -*- coding:UTF-8 -*-
# Author  : LinYaoHong
# Date    : 2020/11/19 9:00 下午
# Filename: orm_test.py
# Tools   : PyCharm
# 在Python脚本中调用Django环境

import os

if __name__ == '__main__':
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "DjangoRestFramework.settings")  # 加载 Django项目的配置信息 manage.py 里也有这一行代码
    import django  # 导入Django并启动

    django.setup()

    from orm import models


    def printf(message='', data=''):
        print(f'\033[31m{message} :\033[0m', data)


    def printg(message='', data=''):
        print(f'\033[32m{message} :\033[0m', data)


    def printb(message='', data=''):
        print(f'\033[36m{message} :\033[0m', data)


    # 创建数据
    # models.Publisher.objects.create(name="中国出版社", addr="中国")
    # data = {"name": "济南出版社", "addr": "济南"}
    # models.Publisher.objects.create(**data)

    # 删除
    # models.Publisher.objects.get(id=1).delete()

    # (1)- all 查询所有的 出版社 QuerySet  有列表所有的操作方法
    ret = models.Publisher.objects.all()
    printf("all", ret)

    # (2) - get 查询如果没有则报错
    # (3) - filter会返回 空 <QuerySet []>
    # get 只返回一条数据, filter如果有多条值会返回多个
    # 查询 id =1
    ret = models.Publisher.objects.filter(id=1)  # <QuerySet [<Publisher: Publisher object (1)>]>
    printf('filter', ret)
    ret = models.Publisher.objects.filter(id=100)  # 不存在 <QuerySet []>
    printf('filter', ret)
    ret = models.Publisher.objects.filter(id=1).first()  # Publisher object (1)
    printf('filter.first', ret)
    ret = models.Publisher.objects.get(id=1)  # Publisher object (1)
    printf('get', ret)
    ret = models.Publisher.objects.first()  # Publisher object (1)
    printf('first', ret)

    ret = models.Publisher.objects.filter(name__isnull=True)
    printf('filter name__isnull', ret)

    # 查询 id为 1 的name 字段的值 取值,不存在都会报错
    ret = models.Publisher.objects.filter(id=1)[0].name
    printf('filter(id=1)[0].name', ret)  # 中国出版社

    ret = models.Publisher.objects.get(id=1).name
    printf('get(id=1).name', ret)  # 中国出版社

    ret = models.Publisher.objects.filter(id=1).first().name
    printf("first().name", ret)  # 中国出版社

    # (4) - exclude 排除 id=2的
    ret = models.Publisher.objects.exclude(id=2)
    printf('exclude', ret)

    # (5) - values 返回一个  QuerySet 对象 里面为字典 格式为 [{},{}]  不写 values则返回全部
    ret = models.Publisher.objects.values('id', 'name')
    printf('values', ret)

    """
    ret :  <QuerySet [{'name': '重庆出版社'}, {'name': '临沂出版社'}, {'name': '济南出版社'}, {'name': '中国出版社'}]>
    如果需要返回 QuerySet 列表 则用 JsonResponse

    def json_values(content):
        data = {
            "code": 0,
            "data": list(content)
        }
        return JsonResponse(data)
        
    return  ret
    """
    # (6)- values_list 返回一个  QuerySet 对象 里面为元祖 格式为 [(),()]  不写 values则返回全部
    ret = models.Publisher.objects.values_list('id', 'name')
    # <QuerySet [(4, '重庆出版社'), (3, '临沂出版社'), (2, '济南出版社'), (1, '中国出版社')]>
    printf('values_list', ret)

    # (7) - order_by 对查询结果排序
    # (8) - reverse 对有序的结果反转排序
    printf("order_by", models.Publisher.objects.filter().order_by("addr"))
    printf("reverse", models.Publisher.objects.filter().order_by("addr").reverse())

    # 直接使用的话 需要在 Meta里指定一个有顺序的结果, 才能reverse 反转排序
    ret = models.Publisher.objects.filter().reverse()  # Meta里已有, 所以可以直接反转排序ordering = ('-id',)
    printf('Meta里自带order_by', ret)
    ret = models.Publisher.objects.filter()
    printf('reverse', ret)

    # (9) - distinct(): 从返回结果中剔除重复纪录
    # (如果你查询跨越多个表，可能在计算QuerySet时得到重复的结果, 此时可以使用distinct()，注意只有在PostgreSQL中支持按字段去重。)

    # (10) - count 返回数据库中匹配查询(QuerySet)的对象数量
    ret = models.Publisher.objects.all().count()
    printf('count', ret)

    # (11) - first  返回第一条记录
    ret = models.Publisher.objects.first()
    printf('first', ret)

    # (12) - last 返回最后一条记录
    ret = models.Publisher.objects.last()
    printf('last', ret)

    # (13) - exists():    如果QuerySet包含数据，就返回True，否则返回False
    ret = models.Publisher.objects.filter(id=1111).exists()
    printf('exists', ret)

    # id__gt id>1 并且 id< 4
    ret = models.Publisher.objects.filter(id__gt=1, id__lt=4)
    printf('_gt_lt', ret)

    # id__in = 2,4
    ret = models.Publisher.objects.filter(id__in=[2, 4])
    printf('id__in', ret)

    # exclude & id__in  ====== not ni
    ret = models.Publisher.objects.exclude(id__in=[2, 4])
    printf('exclude & id__in', ret)

    # name__contains   获取name字段包含"济"的
    ret = models.Publisher.objects.filter(name__icontains='济')  # 大小写不敏感
    ret = models.Publisher.objects.filter(name__contains='济')
    printf('name__contains', ret)

    # id__range  id范围是1到3的，等价于SQL的 between and
    ret = models.Publisher.objects.filter(id__range=[2, 4])
    printf('id__range', ret)

    # startswith 判断某个字段的值是否是以某个值开始的。大小写敏感
    ret = models.Publisher.objects.filter(name__startswith="A")
    printf('startswith', ret)

    # istartswith 判断某个字段的值是否是以某个值开始的。但是大小写是不敏感的
    ret = models.Publisher.objects.filter(name__istartswith="A")
    printf('startswith', ret)

    # endswith   判断某个字段的值是否以某个值结束。大小写敏感
    # iendswith  判断某个字段的值是否以某个值结束。大小写不敏感

    # 日期和时间字段还可以有以下写法
    ret = models.User.objects.filter(create_time__month=11)
    printf('create_time__year: 日期字段-月', ret)
    ret = models.User.objects.filter(create_time__year=2010, create_time__month=1)
    printf('create_time__year: 日期字段-年', ret)

    # 修改/更新-1
    publisher_obj = models.Publisher.objects.get(id=1)
    publisher_obj.name = "中国出版社1"
    publisher_obj.save()
    # 修改/更新-2
    models.Publisher.objects.filter(id=1).update(name="中国出版社")

    print("================================外键================================")

    printf('正向查找')
    # 用get 不能用 filter
    # 基于对象 跨表查询
    book_obj = models.Book.objects.get(id=1)  # id为1的书对象  Book object (1)
    print(book_obj, type(book_obj))
    printg('ForeignKey_正向查找', book_obj.title)  # 书的名字  --> 第一本书
    printg('ForeignKey_正向查找', book_obj.publisher)  # 书关联的出版社对象 -->  Publisher object (1)
    printg('ForeignKey_正向查找', book_obj.publisher.name)  # 书关联的出版社名称  --> 中国出版社

    ret = models.Book.objects.get(id=1).title
    print(ret)
    ret = models.Book.objects.get(id=1).publisher  # Publisher object (1)
    print(ret)
    ret = models.Book.objects.get(id=1).publisher.name
    print(ret)

    # 利用 双 __跨表查询
    ret = models.Book.objects.filter(id=1).values('publisher__name')  # <QuerySet [{'publisher__name': '中国出版社'}]>
    printg('双 __ 跨表查询', ret)

    # 正向查找字段跨表  查找出版社id=2的 书籍
    ret = models.Book.objects.filter(publisher_id=2).count()
    printg('ForeignKey_正向跨字段', ret)  # 个数
    ret = models.Book.objects.filter(publisher__name='济南出版社').values()
    printg('ForeignKey_正向跨字段', ret)  # 列表-字典
    ret = models.Book.objects.filter(publisher_id=2).values_list()
    printg('ForeignKey_正向跨字段', ret)  # 列表-元祖
    ret = models.Book.objects.filter(publisher_id=2).values('id', 'title')
    printg('ForeignKey_正向跨字段', ret)  # 列表-字典

    printf('反向查找')
    """
    仅设置 related_name = "book"
    publisher = models.ForeignKey(to="Publisher", on_delete=models.CASCADE, related_name="book", )
    """
    ret = models.Publisher.objects.get(id=1).book.all()
    printb('反向查找', ret)
    ret = models.Publisher.objects.get(id=1).book.all().values()
    printb('反向查找', ret)
    ret = models.Publisher.objects.filter(book__title="第2本书")
    printb('反向__跨字段过滤( 出版社 是否包含:第2本书)', ret)
    ret = models.Publisher.objects.filter(id=1).values('id', 'name', 'book__title')
    printb('反向__跨字段查询', ret)

    """
    ForeignKey 不设置 related_name="book", 用表名_set 来反向查询 (设置了related_name就不用了)
    publisher = models.ForeignKey(to="Publisher", on_delete=models.CASCADE)
    """


    def test01():
        publisher_ob = models.Publisher.objects.get(id=1)  # 找到第一个出版社对象 Publisher object (60)
        rets = publisher_ob.book_set.all()  # 找到第一个出版社出版的所有书
        printb('反向查找', rets)
        rets = publisher_ob.book_set.all().values('id', 'title')
        printb('反向查找', rets)


    """
    查询 出版社是否 有 第2本书
    related_query_name  和 related_name 
    如果设置了 related_name="book", related_query_name="abc"
    publisher = models.ForeignKey(to="Publisher", on_delete=models.CASCADE, related_name="book", 
                                    related_query_name="abc")
    """


    def test():
        rets = models.Publisher.objects.filter(abc__title="第2本书")
        print(rets)
        # 不设置 related_query_name="abc"
        # rets = models.Publisher.objects.filter(book__title="第2本书")、
        print(rets)


    printf("================================ManyToManyField================================")

    # 查询
    # 查找 作者id为1 写了哪些书
    author_obj = models.Author.objects.get(id=1)
    printg(author_obj.name)
    printg(type(author_obj.book))
    printg(author_obj.book.filter())
    printg(author_obj.book.all())

    # create
    # 通过作者创建一本书
    # author_obj = models.Author.objects.get(id=1)
    # author_obj.book.create(title='书自动创建', price=3.11, publisher_id=1)

    # add 不会覆盖原有的
    author_obj = models.Author.objects.get(id=3)  # 作者
    book_obj = models.Book.objects.get(id=1)  # 书籍
    author_obj.book.add(book_obj)

    book_obj = models.Book.objects.filter(id__gt=5)  # 书籍
    printf(book_obj)
    print(*book_obj)  # *book_obj 把列表打散
    author_obj.book.add(*book_obj)
    author_obj.book.add(2, 3)

    # set clear 作者添加书籍 会清空原有的关联   # set 使用列表
    author_obj.book.set([])
    author_obj.book.set([1, 2, 3, 4, 5])
    # author_obj.book.clear()

    # remove 删除一个 使用 对象/(1,2,4)/*[1,2,3]
    # book_obj = models.Book.objects.get(title="第3本书")
    book_obj2 = models.Book.objects.filter(title="第2本书").first()  # 推荐用filter+first
    # author_obj.book.remove(book_obj)
    # author_obj.book.remove(book_obj2)  # 如果移除对象不存在不会报错
    # author_obj.book.remove(*[4, 5, 6, 7])  # 如果移除对象不存在不会报错
    # author_obj.book.remove(4, 5, 6, 7)  # 如果移除对象不存在不会报错

    # ————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
    #  ForeignKey 反向创建 / 通过出版社创建一本书
    publisher_obj = models.Publisher.objects.filter(id=1).first()
    printf(publisher_obj)
    data = {'title': '书自动创建',
            'price': 3.11,
            'publisher_id': 3}
    # publisher_obj.book.create(**data)

    #  ForeignKey 删除 id 为3 的出版社下的 所有的书籍(前提是ForeignKey可以为空)
    publisher_obj = models.Publisher.objects.filter(id=3).first()
    # publisher_obj.book.clear()  # 把属于出版社3的书籍的关联关系全部删掉

    # 知道出版社对象, 知道书的ID 如果这本书关联了这个出版社/则删除
    ret = publisher_obj.book.all().values('id')
    print(ret)
    for i in ret:
        val = i['id']
        if val == 59:
            book_obj = models.Book.objects.filter(id=val).first()
            publisher_obj.book.remove(book_obj)

    printf("———————————————————————————————————————————————————————————————————————————————————————————————")
    # 分组聚合查询
    from django.db.models import Avg, Sum, Max, Min, Count

    # 平均值 和 最小值
    ret = models.Book.objects.filter().aggregate(price_avg=Avg('price'), price_sum=Sum('price'), price_min=Min('price'))
    print(ret)

    #  annotate  分组
    # 求 每本书的作者数量
    print(11111)
    ret = models.Book.objects.all().annotate(author_num=Count('author'))
    print(ret)
    for i in ret:
        print('1111',i.author_num)

    # 求 作者数量 > 1的 书籍
    ret = models.Book.objects.all().annotate(author_num=Count('author')).filter(author_num__gt=1)
    print(ret)

    # 查询各个作者出的书的总价

    ret = models.Author.objects.all().annotate(price_sum=Sum('book__price')).values('name', 'price_sum')

    print(ret)

    # F 和 Q

    ret = models.Book.objects.filter(price__gt=9.9)
    print(ret)

    from django.db.models import F

    # 库存数 大于卖出数量
    # inventory
    # sales 卖出数
    ret = models.Book.objects.filter(inventory__gt=F('sales'))
    print(ret)

    # 具体的对象没有update()  QuerySet才有update()方法

    # 所有卖出数量 * 3
    # models.Book.objects.update(sales=F("sales") * 3)
    # models.Book.objects.update(sales=(F("sales")+1) * 3) # +1 防止 0 没效果
    from django.db.models.functions import Concat
    from django.db.models import Value

    models.Book.objects.update(title=Concat(F('title'), Value('第一')))

    # Q 查询
    # 库存 > 1000 并且价格 < 100
    ret = models.Book.objects.filter(inventory__gt=1000, price__lt=100)
    print(ret)
    from django.db.models import Q

    # 库存数 > 1000 或 价格 < 100
    ret = models.Book.objects.filter(Q(inventory__gt=1000) | Q(price__lt=100))
    print(ret)

    # 库存数 > 1000 或 价格 < 100 并且书名 包含2的    Q 查询一定写在前面
    ret = models.Book.objects.filter(Q(inventory__gt=1000) | Q(price__lt=100), title__contains = '2')
    print(ret)

    #
