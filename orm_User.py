# -*- coding:UTF-8 -*-
# Author  : LinYaoHong
# Date    : 2020/11/19 9:00 下午
# Filename: orm_test.py
# Tools   : PyCharm
# 在Python脚本中调用Django环境

import os

if __name__ == '__main__':
    # 加载 Django项目的配置信息
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "DjangoRestFramework.settings")
    # 导入Django并启动
    import django

    django.setup()

    from orm import models

    # id = models.AutoField(primary_key=True)
    # type = models.IntegerField(choices=CHOICES, default=4, verbose_name="用户类型 ")
    # name = models.CharField(max_length=50, verbose_name="姓名")
    # age = models.IntegerField(null=True, blank=True, verbose_name="年龄")
    # pri_ip = models.GenericIPAddressField(protocol='ipv4', verbose_name="内网IP")
    # pub_ip = models.GenericIPAddressField(unpack_ipv4=True, protocol="both", verbose_name="外网IP")
    # note = models.TextField(null=True, verbose_name="备注")
    # price = models.DecimalField(null=True, max_digits=5, decimal_places=2)
    # is_active = models.BooleanField(default=True, verbose_name="是否激活状态")  # 0 True 1 False

    data = {
        "type": 2,
        "name": '蔺要红',
        "nickname": "研发",
        "age": '18',
        "pri_ip": "::ffff:192.168.1.1",
        "pub_ip": "::ffff:192.1.1.1",
        "note": "我是备注",
        "price": 111.911119,  # 数据库字段会被存为: 111.91 # max_digits=5, decimal_places=2 只能传入 3位整数, 要给小数点留够2位长度
        "is_active": 1
    }
    models.User.objects.create(**data)
    models.User.objects.filter(id=1).update(nickname="中国", age=11)
