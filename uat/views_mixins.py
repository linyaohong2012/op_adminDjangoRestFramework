# -*- coding: utf-8 -*-

from uat import models
from uat import serializers
from rest_framework import mixins
from rest_framework import generics


# 使用混合（mixins）

class PublisherList(mixins.ListModelMixin,
                    mixins.CreateModelMixin,
                    generics.GenericAPIView):
    """
    列出所有的 Publisher 或者创建一个新的 Publisher
    """

    queryset = models.Publisher.objects.all()
    serializer_class = serializers.PublisherSerializer

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)


class PublisherDetail(mixins.RetrieveModelMixin,
                      mixins.UpdateModelMixin,
                      mixins.DestroyModelMixin,
                      generics.GenericAPIView):
    """
    检索，更新或删除一个Publisher 示例
    """
    queryset = models.Publisher.objects.all()
    serializer_class = serializers.PublisherSerializer

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)

    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)
