from rest_framework import serializers
from uat import models


class PublisherSerializer(serializers.ModelSerializer):
    operator = serializers.ReadOnlyField(source="operator.username")

    class Meta:
        model = models.Publisher
        fields = ("id", "name", "address", "operator")


# 外键 显示超连接
# class BookSerializer(serializers.HyperlinkedModelSerializer):
#     class Meta:
#         model = models.Book
#         fields = ("id", "title", "publisher")


class BookSerializer(serializers.HyperlinkedModelSerializer):
    publisher = serializers.StringRelatedField(source='publisher.name')

    class Meta:
        model = models.Book
        fields = ("id", "title", "publisher")
