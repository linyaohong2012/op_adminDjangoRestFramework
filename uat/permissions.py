# -*- coding:UTF-8 -*-
# Author  : LinYaoHong
# Date    : 2020/11/30 12:03 下午
# Filename: permissions.py
# Tools   : PyCharm


from rest_framework import permissions


class IsOwnerOrReadOnly(permissions.BasePermission):
    """
    自定义权限只允许对象的所有者编辑它。
    """

    def has_object_permission(self, request, view, obj):
        # 读取权限允许任何请求，
        # 所以我们总是允许GET，HEAD 或 OPTIONS请求。
        if request.method in permissions.SAFE_METHODS:
            return True

        # 只有该 publisher 的所有者才允许写权限
        return obj.operator == request.user
