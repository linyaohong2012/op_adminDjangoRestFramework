# -*- coding:UTF-8 -*-
"""

https://www.django-rest-framework.org/

"""
from rest_framework.routers import DefaultRouter
from uat.views import (
    PublisherViewSet,
    BookViewSet
)

router = DefaultRouter()
urlpatterns = [
    router.register("publisher", PublisherViewSet, "publisher"),
    router.register("book", BookViewSet, "book"),
]
