# -*- coding: utf-8 -*-

"""
AllowAny：允许无限制访问
IsAuthenticated ：允许访问任何经过身份验证的用户，并拒绝访问任何未经身份验证的用户
IsAdminUser：允许超级用户访问
IsAuthenticatedOrReadOnly：对经过身份验证的用户的允许完全访问，但对未经身份验证的用户的允许只读访问
"""

from uat import models
from uat import serializers
from rest_framework import permissions
from rest_framework import viewsets

from uat.permissions import IsOwnerOrReadOnly


class PublisherViewSet(viewsets.ModelViewSet):
    queryset = models.Publisher.objects.all()
    serializer_class = serializers.PublisherSerializer
    # permission_classes = (permissions.IsAuthenticated,)  # //IsAuthenticated 必须是一个登陆用户 / 读
    permission_classes = (permissions.IsAuthenticated, IsOwnerOrReadOnly)  # // 所有者可以修改, 其他人只读

    # 重写 operator  create方法,  operator 可以获取到用户id
    def perform_create(self, serializer):
        serializer.save(operator=self.request.user)


class BookViewSet(viewsets.ModelViewSet):
    queryset = models.Book.objects.all()
    serializer_class = serializers.BookSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)  # // 非登陆用户只读
