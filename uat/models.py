from django.db import models


# Create your models here.


class Publisher(models.Model):
    name = models.CharField(max_length=32, verbose_name="出版社名称", unique=True)
    address = models.CharField(max_length=128, verbose_name="出版社地址")
    operator = models.ForeignKey("auth.User", on_delete=models.CASCADE)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "出版社"
        ordering = ('id',)
        verbose_name_plural = verbose_name


class Book(models.Model):
    title = models.CharField(max_length=32, verbose_name="书名")
    publisher = models.ForeignKey(to=Publisher, on_delete=models.CASCADE, null=True, verbose_name='出版社')

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = "书"
        ordering = ('id',)
        verbose_name_plural = verbose_name
