# -*- coding: utf-8 -*-

from uat import models
from uat import serializers
from rest_framework.views import APIView
from rest_framework import status
from django.http import Http404
from rest_framework.response import Response

# 使用基于类的视图重写我们的API

"""
urls.py
from django.urls import path
from django.conf.urls import url
from uat import views

urlpatterns = [
    url(r'^publisher/$', views.PublisherList.as_view(), name="publisher-list"),
    url(r'^publisher/(?P<pk>[0-9]+)/$', views.PublisherDetail.as_view(), name="publisher-detail"),
]

"""


class PublisherList(APIView):
    """
    列出所有的 Publisher 或者创建一个新的 Publisher
    """

    def get(self, request, format=None):
        queryset = models.Publisher.objects.all()
        serializer = serializers.PublisherSerializer(queryset, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = serializers.PublisherSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class PublisherDetail(APIView):
    """
    检索，更新或删除一个Publisher 示例
    """

    def get_object(self, pk):
        try:
            return models.Publisher.objects.get(pk=pk)
        except models.Publisher.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        queryset = self.get_object(pk)
        serializer = serializers.PublisherSerializer(queryset)
        return Response(serializer.data)

    # 修改出版社信息
    def put(self, request, pk, format=None):
        queryset = self.get_object(pk)
        serializer = serializers.PublisherSerializer(queryset, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    # 删除出版社信息
    def delete(self, request, pk, format=None):
        queryset = self.get_object(pk)
        queryset.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
