"""
-*- coding:UTF-8 -*-
Author  : LinYaoHong
Date    : 2020/11/22 9:07 下午
Filename: init.py
Tools   : PyCharm
"""
from django.conf import settings
from django.core.management import execute_from_command_line
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    help = '初始化数据库'

    def handle(self, *args, **options):
        args = ['manage.py', 'makemigrations']
        apps = [x.split('.')[-1] for x in settings.INSTALLED_APPS if x.startswith('.')]
        print(apps)
        execute_from_command_line(args + apps)
        execute_from_command_line(['manage.py', 'migrate'])
        self.stdout.write(self.style.SUCCESS('初始化成功'))
