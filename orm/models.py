"""

    '''
    '''

"""

from django.db import models
import django.utils.timezone as timezone


class FixedCharField(models.Field):
    """
    自定义的char类型的字段类
    """

    def __init__(self, max_length, *args, **kwargs):
        super().__init__(max_length=max_length, *args, **kwargs)
        self.length = max_length

    def db_type(self, connection):
        """
        限定生成数据库表的字段类型为char，长度为length指定的值
        """
        return 'char(%s)' % self.length


class User(models.Model):
    CHOICES = (
        (1, '百度与'),
        (2, '亚马逊'),
        (3, '腾讯云'),
        (4, '阿里云'),
    )

    id = models.AutoField(primary_key=True)  # # unique 唯一的不为空的字段
    type = models.IntegerField(choices=CHOICES, default=4, verbose_name="用户类型 ")

    # null = True  可以传入 'name':'' 数据库则为 空白
    # null = True 不传入 name 数据库则为 null

    # blank=True, null=True   "name": "" 数据库为空白
    # blank=True, null=True   不传入  数据库为空白
    name = models.CharField(null=True, max_length=50, verbose_name="姓名")

    # # 自定义数据类型 char类型 长度为2 只能输入2个汉字, 多了报错
    nickname = FixedCharField(max_length=2, default='运维')

    age = models.IntegerField(null=True, verbose_name="年龄")
    # 除Django admin 没有实质性限制,（数据库层面没有用）
    pri_ip = models.GenericIPAddressField(protocol='ipv4', verbose_name="内网IP", db_index=True)

    # 除Django admin 没有实质性限制,（数据库层面没有用） 输入::ffff:192.0.2.1时候，可解析为192.0.2.1
    pub_ip = models.GenericIPAddressField(unpack_ipv4=True, protocol="both", verbose_name="外网IP")
    note = models.TextField(null=True, verbose_name="备注")

    # "price": 111.911119,  # 数据库字段会被存为: 111.91 # max_digits=5, decimal_places=2 只能传入 3位整数, 要给小数点留够2位长度
    price = models.DecimalField(null=True, max_digits=5, decimal_places=2, verbose_name="没小时工资")
    is_active = models.BooleanField(default=True, verbose_name="是否激活状态")  # 0 True 1 False

    create_time = models.DateField(auto_now_add=True, verbose_name='创建时间')
    update_time = models.DateField(auto_now=True, verbose_name='更新时间')
    c_time = models.DateTimeField(auto_now_add=True, verbose_name="创建时间")
    up_time = models.DateTimeField(auto_now=True, verbose_name='更新时间')

    """
    对于时区小8个小时问题需要设置 USE_TZ = False
    LANGUAGE_CODE = "en-us"
    TIME_ZONE = "Asia/Shanghai"
    USE_I18N = True
    USE_L10N = True
    USE_TZ = False
    """

    # def __str__(self):
    #     return self.name

    # 原数据
    class Meta:
        db_table = "user"  # 指定表名字
        ordering = ['create_time']  # 指定默认按什么字段排序 只有设置了该属性，我们查询到的结果才可以被reverse()
        verbose_name = "用户表"  # 一般用来作中文解释  admin显示中文

        # 复数名称，因中文没有复数，但django有时又会将用户的驼峰命名拆成单个词，给最后的词加复数，和用户的本义不符，
        # 因些加了这样一个选项来处理尴尬
        verbose_name_plural = verbose_name
        # index_together = ["nickname", "name"]   #  index_together 联合索引
        # unique_together = ['pri_ip', 'pub_ip']  #  unique_together 联合唯一索引


class Publisher(models.Model):
    name = models.CharField(null=True, max_length=64, unique=True, verbose_name='出版社名称')
    addr = models.CharField(max_length=128)

    class Meta:
        db_table = "publisher"
        ordering = ('-id',)
        verbose_name = "增值服务"

        verbose_name_plural = verbose_name


#  https://www.cnblogs.com/gwklan/p/11140079.html

class Book(models.Model):
    title = models.CharField(max_length=64, null=True)
    price = models.DecimalField(max_digits=5, decimal_places=2, default=0)
    inventory = models.IntegerField(default=10000, verbose_name="库存")
    sales = models.IntegerField(default=0, verbose_name="卖出储量")
    # ForeignKey表示外键  字段为 publisher_id
    # publisher = models.ForeignKey(to="Publisher", on_delete=models.CASCADE)
    publisher = models.ForeignKey(to="Publisher", null=True, on_delete=models.CASCADE, related_name="book")

    # publisher = models.ForeignKey(to="Publisher", on_delete=models.CASCADE, related_name="book",
    #                               related_query_name="abc")

    # publisher = models.ForeignKey(to="Publisher", on_delete=models.CASCADE, db_column="publisher_id")

    class Meta:
        db_table = "book"  # 指定表名字

    """  
        related_name='book'         反向操作时，使用的字段名，用于代替原反向查询时的'表名_set'  // models.Publisher.objects.get(id=1).book.all()
        related_query_name="abc"    反向双下划线跨表查询,用来代替表名  // models.Publisher.objects.filter(abc__title="第2本书")
        db_constraint=False/True    是否在数据库中创建外键约束，默认为True 软外键, 数据库不做约束, 需要代码控制 
        db_column=                  自定义字段名 指定外键字段的名字 默认为： publisher --> publisher_id ( 字段_id)   
        to_field=                   设置要关联的表的字段 (了解),默认关联字段为 id
     
        on_delete=
            models.CASCADE       删除关联数据，与之关联也删除
            models.DO_NOTHING    删除关联数据，引发错误IntegrityError
            models.PROTECT       删除关联数据，引发错误ProtectedError
            models.SET_NULL      删除关联数据，与之关联的值设置为null（前提FK字段需要设置为可空）
            models.SET_DEFAULT   删除关联数据，与之关联的值设置为默认值（前提FK字段需要设置默认值）
    """


class Author(models.Model):
    name = models.CharField(null=False, max_length=16, unique=True)
    book = models.ManyToManyField(to='book', related_name='author', verbose_name="关联的书籍")

    class Meta:
        db_table = "author"
