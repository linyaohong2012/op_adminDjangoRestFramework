"""

------------------------------------ Django ORM 常用字段  ------------------------------------

AutoField(Field)
    - int自增列，必须填入参数 primary_key=True

BigAutoField(AutoField)
    - bigint自增列，必须填入参数 primary_key=True

    注：当model中如果没有自增列，则自动会创建一个列名为id的列
    from django.db import models

    class UserInfo(models.Model):
        # 自动创建一个列名为id的且为自增的整数列
        username = models.CharField(max_length=32)

    class Group(models.Model):
        # 自定义自增列
        nid = models.AutoField(primary_key=True)
        name = models.CharField(max_length=32)

SmallIntegerField(IntegerField):
    - 小整数 -32768 ～ 32767

PositiveSmallIntegerField(PositiveIntegerRelDbTypeMixin, IntegerField)
    - 正小整数 0 ～ 32767
IntegerField(Field)
    - 整数列(有符号的) -2147483648 ～ 2147483647

PositiveIntegerField(PositiveIntegerRelDbTypeMixin, IntegerField)
    - 正整数 0 ～ 2147483647

BigIntegerField(IntegerField):
    - 长整型(有符号的) -9223372036854775808 ～ 9223372036854775807

BooleanField(Field)
    - 布尔值类型

NullBooleanField(Field):
    - 可以为空的布尔值

CharField(Field)
    - 字符类型
    - 必须提供max_length参数， max_length表示字符长度

TextField(Field)
    - 文本类型

EmailField(CharField)：
    - 字符串类型，Django Admin以及ModelForm中提供验证机制

IPAddressField(Field)  #  2.0已经被废弃,转为使用: GenericIPAddressField
    - 字符串类型，Django Admin以及ModelForm中提供验证 IPV4 机制

GenericIPAddressField(Field)
    - 字符串类型，Django Admin以及ModelForm中提供验证 Ipv4和Ipv6
    - 参数：2者只能选一个
        protocol='ipv4' 用于指定Ipv4或Ipv6， 'both',"ipv4","ipv6"
        unpack_ipv4=True, protocol="both" 则输入::ffff:192.0.2.1时候，可解析为192.0.2.1

URLField(CharField)
    - 字符串类型，Django Admin以及ModelForm中提供验证 URL

SlugField(CharField)
    - 字符串类型，Django Admin以及ModelForm中提供验证支持 字母、数字、下划线、连接符（减号）

CommaSeparatedIntegerField(CharField)
    - 字符串类型，格式必须为逗号分割的数字

UUIDField(Field)
    - 字符串类型，Django Admin以及ModelForm中提供对UUID格式的验证

FilePathField(Field)
    - 字符串，Django Admin以及ModelForm中提供读取文件夹下文件的功能
    - 参数：
            path,                      文件夹路径
            match=None,                正则匹配
            recursive=False,           递归下面的文件夹
            allow_files=True,          允许文件
            allow_folders=False,       允许文件夹

FileField(Field)
    - 字符串，路径保存在数据库，文件上传到指定目录
    - 参数：
        upload_to = ""      上传文件的保存路径
        storage = None      存储组件，默认django.core.files.storage.FileSystemStorage

ImageField(FileField)
    - 字符串，路径保存在数据库，文件上传到指定目录
    - 参数：
        upload_to = ""      上传文件的保存路径
        storage = None      存储组件，默认django.core.files.storage.FileSystemStorage
        width_field=None,   上传图片的高度保存的数据库字段名（字符串）
        height_field=None   上传图片的宽度保存的数据库字段名（字符串）

DateTimeField(DateField)
    - 日期+时间格式 YYYY-MM-DD HH:MM[:ss[.uuuuuu]][TZ]

DateField(DateTimeCheckMixin, Field)
    - 日期格式      YYYY-MM-DD

TimeField(DateTimeCheckMixin, Field)
    - 时间格式      HH:MM[:ss[.uuuuuu]]

DurationField(Field)
    - 长整数，时间间隔，数据库中按照bigint存储，ORM中获取的值为datetime.timedelta类型

FloatField(Field)
    - 浮点型

DecimalField(Field)
    - 10进制小数
    - 参数：
        max_digits，小数总长度
         ，小数位长度

BinaryField(Field)
    二进制类型

"""

"""
------------------------------------ Django ORM 常用参数  ------------------------------------

字段参数
null  用于表示某个字段可以为空

unique  如果设置为unique=True 则该字段在此表中必须是唯一的 。

db_index  如果db_index=True 则代表着为此字段设置数据库索引。

default  为该字段设置默认值

时间字段独有  DatetimeField、DateField、TimeField这个三个时间字段，都可以设置如下属性。
    auto_now_add 配置auto_now_add=True ,创建数据记录的时候会把当前时间添加到数据库。
    auto_now  配置上auto_now=True，每次更新数据记录的时候会更新该字段。

------------------------------------------------------------------------------------------

null = True  
    Django将在数据库中存储一个空值NULL。默认为 False
    主要是用在IntegerField，DateField, DateTimeField，这几个字段不接受空字符串，所以在使用时，必须将blank和null同时赋值为True

blank = True
    经过验证  blank = True 没什么鸟用, 可能方法不对 用的 django 语法, 没有使用django Admin
    
    则允许该字段为空白。默认为False
    blank主要是用在CharField, TextField，这两个字符型字段可以用空字符穿来储存空值
    与null是不同的，null纯粹是与数据库相关的。而blank则与验证相关。
    如果一个字段设置为blank = True，表单验证时允许输入一个空值。而blank = False，则该项必需输入数据


-----------------------------  ForeignKey-------------------------------------------------------------
-----------------------------  OneToOneField-------------------------------------------------------------
-----------------------------  ManyToManyField-------------------------------------------------------------


svc = models.OneToOneField(to="SvcInfo", related_name='+', null=True, on_delete=models.CASCADE,verbose_name="服务器详情")
ui_name = models.ManyToManyField(to=User, blank=True, verbose_name="后端", related_name="+")
created_by = models.ForeignKey(User, models.PROTECT, related_name='+', null=True)
"""
